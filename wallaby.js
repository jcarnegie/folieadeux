module.exports = function wallabyConfig(wallaby) {
  return {
    files: [
      'src/**/*.js',
    ],
    tests: [
      'test/unit/**/*.test.js',
    ],
    env: {
      type: 'node',
      runner: 'node',
    },
    compilers: {
      '**/*.js': wallaby.compilers.babel(),
    },
    testFramework: 'jest',
  };
};
