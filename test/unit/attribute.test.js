// @flow
import Sequelize from 'sequelize';
import { parse } from 'graphql/language';
import { createAttribute } from '../../src/attribute';

test('should set type to jsonb for non-scalar/non-model types', () => {
  const field = {
    kind: 'FieldDefinition',
    name: { value: 'foo' },
    type: { name: { value: 'Foo' } },
    arguments: [],
    directives: [],
  };

  const attr = createAttribute(field);

  expect(attr.type).toEqual('jsonb');
  expect(attr.sequelizeType).toEqual(Sequelize.JSONB);
});

test('should set type to jsonb for non-scalar/non-model list types', () => {
  const field = {
    kind: 'FieldDefinition',
    name: { value: 'foo' },
    type: { name: { value: 'Foo' } },
    arguments: [],
    directives: [],
  };

  const attr = createAttribute(field);

  expect(attr.type).toEqual('jsonb');
  expect(attr.sequelizeType).toEqual(Sequelize.JSONB);
});

test('should create relation for belongsTo directive', () => {
  const schema = `
    type A @model {
      foo: String
    }

    type B @model {
      a: A @relation(type: "belongsTo")
    }
  `;

  const ast = parse(schema);
  const bDef = ast.definitions[1];
  const aField = bDef.fields[0];
  const attr = createAttribute(aField);
  expect(attr.relation).toEqual({
    target: 'A',
    type: 'belongsTo',
  });
});

test('should create relation for hasOne directive', () => {
  const schema = `
    type A @model {
      foo: String
    }

    type B @model {
      a: A @relation(type: "hasOne")
    }
  `;

  const ast = parse(schema);
  const bDef = ast.definitions[1];
  const aField = bDef.fields[0];
  const attr = createAttribute(aField);
  expect(attr.relation).toEqual({
    target: 'A',
    type: 'hasOne',
  });
});

test('should create relation for hasMany directive', () => {
  const schema = `
    type A @model {
      foo: String
    }

    type B @model {
      a: A @relation(type: "hasMany")
    }
  `;

  const ast = parse(schema);
  const bDef = ast.definitions[1];
  const aField = bDef.fields[0];
  const attr = createAttribute(aField);
  expect(attr.relation).toEqual({
    target: 'A',
    type: 'hasMany',
  });
});

test('should create relation for belongsToMany directive', () => {
  const schema = `
    type A @model {
      foo: String
    }

    type B @model {
      a: A @relation(type: "belongsToMany", through: "AB")
    }
  `;

  const ast = parse(schema);
  const bDef = ast.definitions[1];
  const aField = bDef.fields[0];
  const attr = createAttribute(aField);
  expect(attr.relation).toEqual({
    target: 'A',
    type: 'belongsToMany',
    through: 'AB',
  });
});

test('should create relation for belongsToMany directive with list type', () => {
  const schema = `
    type A @model {
      foo: String
    }

    type B @model {
      a: [A] @relation(type: "belongsToMany", through: "AB")
    }
  `;

  const ast = parse(schema);
  const bDef = ast.definitions[1];
  const aField = bDef.fields[0];
  const attr = createAttribute(aField);
  expect(attr.relation).toEqual({
    target: 'A',
    type: 'belongsToMany',
    through: 'AB',
  });
});
