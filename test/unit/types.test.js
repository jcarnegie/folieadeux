// import Sequelize from 'sequelize';
import { sqlToSequelize } from '../../src/types';

test('should convert varchar to Sequelize.STRING', () => {
  let type = sqlToSequelize('varchar');
  expect(type.key).toEqual('STRING');

  type = sqlToSequelize('VARCHAR');
  expect(type.key).toEqual('STRING');
});

test('should convert varchar(100) to Sequelize.STRING(100)', () => {
  let type = sqlToSequelize('varchar(100)');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);

  type = sqlToSequelize('VARCHAR(100)');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);

  type = sqlToSequelize('varchar (100)');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);

  type = sqlToSequelize('varchar (100) ');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);

  type = sqlToSequelize('varchar ( 100 )');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);
});

test('should convert varchar binary to Sequelize.STRING.BINARY', () => {
  let type = sqlToSequelize('varchar binary');
  expect(type.key).toEqual('STRING');
  expect(type.options.binary).toEqual(true);

  type = sqlToSequelize('VARCHAR BINARY');
  expect(type.key).toEqual('STRING');
  expect(type.options.binary).toEqual(true);
});

test('should convert varchar(100) binary to Sequelize.STRING(100).BINARY', () => {
  let type = sqlToSequelize('varchar(100) binary');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);
  expect(type.options.binary).toEqual(true);

  type = sqlToSequelize('VARCHAR(100) BINARY');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);
  expect(type.options.binary).toEqual(true);

  type = sqlToSequelize('varchar (100) binary');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);
  expect(type.options.binary).toEqual(true);

  type = sqlToSequelize('varchar (100) binary ');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);
  expect(type.options.binary).toEqual(true);

  type = sqlToSequelize('varchar ( 100 ) binary');
  expect(type.key).toEqual('STRING');
  expect(type.options.length).toEqual(100);
  expect(type.options.binary).toEqual(true);
});

test('should convert text to Sequelize.TEXT', () => {
  let type = sqlToSequelize('text');
  expect(type.key).toEqual('TEXT');

  type = sqlToSequelize('TEXT');
  expect(type.key).toEqual('TEXT');
});

test('should convert tinytext to Sequelize.TEXT(\'tiny\')', () => {
  let type = sqlToSequelize('tinytext');
  expect(type.key).toEqual('TEXT');
  expect(type.options.length).toEqual('tiny');

  type = sqlToSequelize('TINYTEXT');
  expect(type.key).toEqual('TEXT');
  expect(type.options.length).toEqual('tiny');
});

test('should convert integer to Sequelize.INTEGER', () => {
  let type = sqlToSequelize('integer');
  expect(type.key).toEqual('INTEGER');

  type = sqlToSequelize('INTEGER');
  expect(type.key).toEqual('INTEGER');
});

test('should convert bigint to Sequelize.BIGINT', () => {
  let type = sqlToSequelize('bigint');
  expect(type.key).toEqual('BIGINT');

  type = sqlToSequelize('BIGINT');
  expect(type.key).toEqual('BIGINT');
});

test('should convert bigint(10) to Sequelize.BIGINT(10)', () => {
  let type = sqlToSequelize('bigint(10)');
  expect(type.key).toEqual('BIGINT');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('bigint (10)');
  expect(type.key).toEqual('BIGINT');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('BIGINT(10)');
  expect(type.key).toEqual('BIGINT');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('BIGINT ( 10 ) ');
  expect(type.key).toEqual('BIGINT');
  expect(type.options.length).toEqual(10);
});

test('should convert float to Sequelize.FLOAT', () => {
  let type = sqlToSequelize('float');
  expect(type.key).toEqual('FLOAT');

  type = sqlToSequelize('FLOAT');
  expect(type.key).toEqual('FLOAT');
});

test('should convert float(10) to Sequelize.FLOAT(10)', () => {
  let type = sqlToSequelize('float(10)');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('float (10)');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('FLOAT(10)');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('FLOAT ( 10 ) ');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);
});

test('should convert float(10, 10) to Sequelize.FLOAT(10, 10)', () => {
  let type = sqlToSequelize('float(10, 10)');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('float (10,10)');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('FLOAT(10, 10 )');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('FLOAT ( 10 , 10 ) ');
  expect(type.key).toEqual('FLOAT');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);
});

test('should convert real to Sequelize.REAL', () => {
  let type = sqlToSequelize('real');
  expect(type.key).toEqual('REAL');

  type = sqlToSequelize('REAL');
  expect(type.key).toEqual('REAL');
});

test('should convert real(10) to Sequelize.REAL(10)', () => {
  let type = sqlToSequelize('real(10)');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('real (10)');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('REAL(10)');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('REAL ( 10 ) ');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);
});

test('should convert real(10, 10) to Sequelize.REAL(10, 10)', () => {
  let type = sqlToSequelize('real(10, 10)');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('real (10,10)');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('REAL(10, 10 )');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('REAL ( 10 , 10 ) ');
  expect(type.key).toEqual('REAL');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);
});

test('should convert double to Sequelize.DOUBLE', () => {
  let type = sqlToSequelize('double');
  expect(type.key).toEqual('DOUBLE PRECISION');

  type = sqlToSequelize('DOUBLE');
  expect(type.key).toEqual('DOUBLE PRECISION');
});

test('should convert double(10) to Sequelize.DOUBLE(10)', () => {
  let type = sqlToSequelize('double(10)');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('double (10)');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('DOUBLE(10)');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);

  type = sqlToSequelize('DOUBLE ( 10 ) ');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);
});

test('should convert double(10, 10) to Sequelize.DOUBLE(10, 10)', () => {
  let type = sqlToSequelize('double(10, 10)');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('double (10,10)');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('DOUBLE(10, 10 )');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);

  type = sqlToSequelize('DOUBLE ( 10 , 10 ) ');
  expect(type.key).toEqual('DOUBLE PRECISION');
  expect(type.options.length).toEqual(10);
  expect(type.options.decimals).toEqual(10);
});






test('should convert decimal to Sequelize.DECIMAL', () => {
  let type = sqlToSequelize('decimal');
  expect(type.key).toEqual('DECIMAL');

  type = sqlToSequelize('DECIMAL');
  expect(type.key).toEqual('DECIMAL');
});

test('should convert decimal(10, 10) to Sequelize.DECIMAL(10, 10)', () => {
  let type = sqlToSequelize('decimal(10, 10)');
  expect(type.key).toEqual('DECIMAL');
  expect(type.options.precision).toEqual(10);
  expect(type.options.scale).toEqual(10);

  type = sqlToSequelize('decimal (10,10)');
  expect(type.key).toEqual('DECIMAL');
  expect(type.options.precision).toEqual(10);
  expect(type.options.scale).toEqual(10);

  type = sqlToSequelize('DECIMAL(10, 10 )');
  expect(type.key).toEqual('DECIMAL');
  expect(type.options.precision).toEqual(10);
  expect(type.options.scale).toEqual(10);

  type = sqlToSequelize('DECIMAL ( 10 , 10 ) ');
  expect(type.key).toEqual('DECIMAL');
  expect(type.options.precision).toEqual(10);
  expect(type.options.scale).toEqual(10);
});


test('should convert timestamp with time zone to Sequelize.DATE', () => {
  let type = sqlToSequelize('timestamp with time zone');
  expect(type.key).toEqual('DATE');

  type = sqlToSequelize('TIMESTAMP WITH TIME ZONE');
  expect(type.key).toEqual('DATE');
});

test('should convert datetime with time zone to Sequelize.DATE', () => {
  let type = sqlToSequelize('datetime');
  expect(type.key).toEqual('DATE');

  type = sqlToSequelize('DATETIME');
  expect(type.key).toEqual('DATE');
});

test('should convert datetime(6) with time zone to Sequelize.DATE(6)', () => {
  let type = sqlToSequelize('datetime(6)');
  expect(type.key).toEqual('DATE');
  expect(type.options.length).toEqual(6);

  type = sqlToSequelize('datetime(3)');
  expect(type.key).toEqual('DATE');
  expect(type.options.length).toEqual(3);

  type = sqlToSequelize('DATETIME(6)');
  expect(type.key).toEqual('DATE');

  expect(() => {
    sqlToSequelize('DATETIME(7)');
  }).toThrowError('datetime modifier must be between 0 and 6');
});

test('should convert json to Sequelize.JSON', () => {
  let type = sqlToSequelize('json');
  expect(type.key).toEqual('JSON');

  type = sqlToSequelize('JSON');
  expect(type.key).toEqual('JSON');
});

test('should convert jsonb to Sequelize.JSONB', () => {
  let type = sqlToSequelize('jsonb');
  expect(type.key).toEqual('JSONB');

  type = sqlToSequelize('JSONB');
  expect(type.key).toEqual('JSONB');
});

test('should convert blob to Sequelize.BLOB', () => {
  let type = sqlToSequelize('blob');
  expect(type.key).toEqual('BLOB');

  type = sqlToSequelize('BLOB');
  expect(type.key).toEqual('BLOB');
});

test('should convert tinyblob to Sequelize.BLOB(\'tiny\')', () => {
  const type = sqlToSequelize('tinyblob');
  expect(type.key).toEqual('BLOB');
  expect(type.options.length).toEqual('tiny');
});

test('should convert mediumblob to Sequelize.BLOB(\'medium\')', () => {
  const type = sqlToSequelize('mediumblob');
  expect(type.key).toEqual('BLOB');
  expect(type.options.length).toEqual('medium');
});

test('should convert longblob to Sequelize.BLOB(\'long\')', () => {
  const type = sqlToSequelize('longblob');
  expect(type.key).toEqual('BLOB');
  expect(type.options.length).toEqual('long');
});

test('should convert bytea to Sequelize.BLOB', () => {
  let type = sqlToSequelize('bytea');
  expect(type.key).toEqual('BLOB');

  type = sqlToSequelize('BYTEA');
  expect(type.key).toEqual('BLOB');
});

test('should convert uuid to Sequelize.UUID', () => {
  let type = sqlToSequelize('uuid');
  expect(type.key).toEqual('UUID');

  type = sqlToSequelize('UUID');
  expect(type.key).toEqual('UUID');
});

test('should convert geometry to Sequelize.GEOMETRY', () => {
  let type = sqlToSequelize('geometry');
  expect(type.key).toEqual('GEOMETRY');

  type = sqlToSequelize('GEOMETRY');
  expect(type.key).toEqual('GEOMETRY');
});

test('should convert point to Sequelize.GEOMETRY(\'point\')', () => {
  let type = sqlToSequelize('point');
  expect(type.key).toEqual('GEOMETRY');
  expect(type.options.type).toEqual('point');

  type = sqlToSequelize('POINT');
  expect(type.key).toEqual('GEOMETRY');
  expect(type.options.type).toEqual('point');
});
