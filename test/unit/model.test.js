// @flow
import type { DocumentNode, ObjectTypeDefinitionNode } from 'graphql/language';
import { parse } from 'graphql/language';
import { find, length, pathEq } from 'ramda';
import {
  createModelCreateTypedef,
  createModelUpdateTypedef,
  findModelTypes,
  modelMutationFieldDefs,
  modelQueryFieldDefs,
} from '../../src/model';


test('should find model types', () => {
  const schema = `
    type User @model(with: ["list", "create"]) {
      id: ID!
      email: String!
    }

    type NotAModel {
      foo: String
      bar: Integer
    }
  `;

  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);

  expect(length(models)).toEqual(1);
  expect(models[0].name.value).toEqual('User');

  // console.log(JSON.stringify(ast, null, 4));
});

test('should get model view query schema definition', () => {
  const schema = 'type User @model { id: ID! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const resolverDefs: DocumentNode = modelQueryFieldDefs(model);
  const { fields }: $ReadOnlyArray<FieldDefinitionNode> = resolverDefs.definitions[0];

  expect(fields[0].kind).toEqual('FieldDefinition');
  expect(fields[0].name.value).toEqual('viewUser');
  expect(fields[0].arguments[0].name.value).toEqual('id');
  expect(fields[0].arguments[0].type.type.name.value).toEqual('ID');
});

test('should get model list query schema definition', () => {
  const schema = 'type User @model { id: ID! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const resolverDefs: DocumentNode = modelQueryFieldDefs(model);
  const { fields }: $ReadOnlyArray<FieldDefinitionNode> = resolverDefs.definitions[0];

  expect(fields[1].kind).toEqual('FieldDefinition');
  expect(fields[1].name.value).toEqual('listUser');
  expect(fields[1].arguments[0].name.value).toEqual('where');
  expect(fields[1].arguments[0].type.name.value).toEqual('Value');
});

test('should get model delete query schema definition', () => {
  const schema = 'type User @model { id: ID! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const resolverDefs: $ReadOnlyArray<FieldDefinitionNode> = modelMutationFieldDefs(model);
  const { fields }: $ReadOnlyArray<FieldDefinitionNode> = resolverDefs.definitions[0];

  expect(fields[2].kind).toEqual('FieldDefinition');
  expect(fields[2].name.value).toEqual('deleteUser');
  expect(fields[2].arguments[0].name.value).toEqual('id');
  expect(fields[2].arguments[0].type.type.name.value).toEqual('ID');
});

test('should get model create query schema definition', () => {
  const schema = 'type User @model { id: ID!, email: String! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const resolverDefs: DocumentNode = modelMutationFieldDefs(model);
  const { fields }: $ReadOnlyArray<FieldDefinitionNode> = resolverDefs.definitions[0];

  expect(fields[0].kind).toEqual('FieldDefinition');
  expect(fields[0].name.value).toEqual('createUser');
  expect(fields[0].arguments[0].kind).toEqual('InputValueDefinition');
  expect(fields[0].arguments[0].name.value).toEqual('payload');
  expect(fields[0].arguments[0].type.kind).toEqual('NonNullType');
  expect(fields[0].arguments[0].type.type.name.value).toEqual('UserCreatePayload');
});

test('should get model update query schema definition', () => {
  const schema = 'type User @model { id: ID!, email: String! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const resolverDefs: DocumentNode = modelMutationFieldDefs(model);
  const { fields }: $ReadOnlyArray<FieldDefinitionNode> = resolverDefs.definitions[0];

  expect(fields[1].kind).toEqual('FieldDefinition');
  expect(fields[1].name.value).toEqual('updateUser');
  expect(fields[1].arguments[0].kind).toEqual('InputValueDefinition');
  expect(fields[1].arguments[0].name.value).toEqual('id');
  expect(fields[1].arguments[1].kind).toEqual('InputValueDefinition');
  expect(fields[1].arguments[1].name.value).toEqual('payload');
  expect(fields[1].arguments[1].type.kind).toEqual('NonNullType');
  expect(fields[1].arguments[1].type.type.name.value).toEqual('UserUpdatePayload');
});

test('should create model create payload type defs', () => {
  const schema = 'type User @model { id: ID!, email: String! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const typedef: ObjectTypeDefinitionNode = createModelCreateTypedef(model);

  expect(typedef.name.value).toEqual('UserCreatePayload');
  expect(typedef.kind).toEqual('InputObjectTypeDefinition');
  expect(length(typedef.fields)).toEqual(1);
  expect(find(pathEq(['name', 'value'], 'id'), typedef.fields)).toEqual(undefined);
  expect(typedef.fields[0].kind).toEqual('InputValueDefinition');
  expect(find(pathEq(['name', 'value'], 'model'), typedef.directives)).toEqual(undefined);
});

test('should create model update payload type defs', () => {
  const schema = 'type User @model { id: ID!, email: String! }';
  const ast: DocumentNode = parse(schema);
  const models: $ReadOnlyArray<ObjectTypeDefinitionNode> = findModelTypes(ast);
  const model: ObjectTypeDefinitionNode = models[0];
  const typedef: ObjectTypeDefinitionNode = createModelUpdateTypedef(model);

  expect(typedef.name.value).toEqual('UserUpdatePayload');
  expect(typedef.kind).toEqual('InputObjectTypeDefinition');
  expect(length(typedef.fields)).toEqual(2);
  expect(typedef.fields[0].kind).toEqual('InputValueDefinition');
  expect(find(pathEq(['name', 'value'], 'model'), typedef.directives)).toEqual(undefined);
});

test('should find model types', () => {
  const schema = `
    type User @model {
      id: ID!
      email: String!
    }

    type Test {
      foo: String
      bar: Int
    }
  `;
  const ast = parse(schema);
  const modelTypes = findModelTypes(ast);
  expect(modelTypes).toHaveLength(1);
});

// test('blah', () => {
//   const schema = `
//     type Query {
//       foo: Integer
//     }

//     input Test {
//       foo: String
//     }
//   `;

//   const ast = parse(schema);
//   console.log(JSON.stringify(ast, null, 4));
// });
