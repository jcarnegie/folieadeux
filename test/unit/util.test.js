// @flow

import { attempt, sleep } from '../../src/util';

test('should sleep', async () => {
  const start = new Date().getTime();
  await sleep(1000);
  const now = new Date().getTime();
  expect(now - start).toBeGreaterThan(900);
});

test('should attempt', async () => {
  let attemptNumber = 0;
  const fn = async () => {
    ++attemptNumber;
    if (attemptNumber < 2) throw new Error('Failure!');
    return 'somestring';
  };
  const res = await attempt(3, 500, fn);
  expect(res).toEqual('somestring');
  expect(attemptNumber).toEqual(2);
});

test('should timeout attempts', async () => {
  const fn = async () => { throw new Error('Failure!'); };
  expect(attempt(3, 500, fn))
    .rejects
    .toEqual(new Error('Timeout - too many attempts'));
});
