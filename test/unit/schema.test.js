// @flow

import Sequelize from 'sequelize';
import { parse } from 'graphql/language';
// import { buildASTSchema } from 'graphql/utilities';
// import { match } from 'ramda';
import { build as buildSchema } from '../../src/schema';

test('dummy', () => {
  const schema = `
    type Foo { id: ID! }
    type Bar @model { foo: [Foo] }
  `;
});

test('should build schema', () => {
  const typedefs = `
    type User @model {
      id: ID! @primary @increments
      email: String!
    }
  `;

  // const ast = parse('type Query { _: Int }');
  // console.log(JSON.stringify(ast, null, 2));
  // console.log(JSON.stringify(buildASTSchema(ast), null, 2));

  const sequelize = new Sequelize({ dialect: 'postgres' });

  const schema = buildSchema([typedefs], sequelize);

  // console.log(JSON.stringify(schema, null, 4));
});
