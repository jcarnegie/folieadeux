// @flow
import Sequelize from 'sequelize';
import { graphql } from 'graphql';
import { build } from '../../src/schema';
import { PG_USER, PG_PASSWORD, PG_HOST, PG_PORT } from './helpers/db';

const typedefs = `
  type Post @model {
    id: ID! @serialize
    UserId: ID!
    contents: String
  }

  type User @model {
    id: ID! @serialize
    email: String!
    phone: String
    posts: [Post] @relation(type: "hasMany")
  }
`;

let sequelize = null;
let schema = null;

beforeEach(async () => {
  sequelize = new Sequelize('postgres', PG_USER, PG_PASSWORD, { dialect: 'postgres', host: PG_HOST, port: PG_PORT });
  schema = build([typedefs], sequelize);
  await sequelize.sync({ logging: false });
  await global.pg.query('insert into "Users" values ($1, $2, $3, $4, $5)', [1, 'joe@gmail.com', '555-555-5555', new Date(), new Date()]);
  await global.pg.query('insert into "Posts" values ($1, $2, $3, $4, $5)', [1, 1, 'foo barrrr', new Date(), new Date()]);
});

afterEach(async () => {
  await sequelize.drop({ logging: false });
  await sequelize.close();
});

test('should view data', async () => {
  const q = '{ viewUser(id: 1) { id email phone } }';
  const result = await graphql(schema, q);
  expect(result).toEqual({
    data: {
      viewUser: {
        id: '1',
        email: 'joe@gmail.com',
        phone: '555-555-5555',
      },
    },
  });
});

test('should list data', () => {

});

test('should create data', () => {

});

test('should update data', () => {

});

test('should delete data', () => {

});

test('should view relational data', async () => {
  const q = '{ viewUser(id: 1) { id email phone posts { id UserId contents } } }';
  const result = await graphql(schema, q);
  expect(result).toEqual({
    data: {
      viewUser: {
        id: '1',
        email: 'joe@gmail.com',
        phone: '555-555-5555',
        posts: [{ id: '1', UserId: '1', contents: 'foo barrrr' }],
      },
    },
  });
});
