module.exports = {
  globalSetup: '<rootDir>/helpers/setup.js',
  globalTeardown: '<rootDir>/helpers/teardown.js',
  testEnvironment: '<rootDir>/helpers/db-jest-environment.js',
  // testPathIgnorePatterns: ['node_modules', 'deprecated'],
};
