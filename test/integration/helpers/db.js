const shell = require('shelljs');

const CONTAINER_NAME = 'ms-graphql-test-postgres-db';
const PG_PASSWORD = 'pgpassword';
const PG_USER = 'postgres';
const PG_HOST = '127.0.0.1';
const PG_PORT = 6543;

const silent = { silent: true };

const startContainer = () =>
  `docker run -d -p ${PG_PORT}:5432 -e POSTGRES_USER=${PG_USER} -e POSTGRES_PASSWORD=${PG_PASSWORD} --name ${CONTAINER_NAME} postgres`;

const stopContainer = () =>
  `docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}`;

module.exports.start = async () => {
  try {
    shell.exec(stopContainer(), silent);
    const result = shell.exec(startContainer(), silent);
    // console.log(result);
    if (result.code !== 0 && !result.stderr.match(/already in use/)) {
      throw new Error(`Unable to start postgres DB container:\n${result.stderr}${result.stdout}`);
    }
  } catch (e) {
    console.log(e);
  }
};

module.exports.stop = async () => {
  const result = shell.exec(stopContainer(), silent);
  if (result.code !== 0) {
    throw new Error(`Unable to stop postgres DB container:\n${result.stderr}${result.stdout}`);
  }
};

module.exports.PG_USER = PG_USER;
module.exports.PG_PASSWORD = PG_PASSWORD;
module.exports.PG_PORT = PG_PORT;
module.exports.PG_HOST = PG_HOST;
