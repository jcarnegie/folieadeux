const { start, PG_PASSWORD, PG_PORT } = require('./db');
const { connect } = require('../../../src/db');

module.exports = async () => {
  await start();
  global.pg = await connect({ password: PG_PASSWORD, port: PG_PORT });
};
