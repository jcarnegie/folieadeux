const { stop } = require('./db');

module.exports = async () => {
  await global.pg.end();
  console.log('Postgres connection ended');
  await stop();
  console.log('Stopped postgres docker container');
};
