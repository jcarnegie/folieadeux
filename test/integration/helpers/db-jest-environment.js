/* eslint-disable no-console,no-underscore-dangle */
const NodeEnvironment = require('jest-environment-node');

console.log('configuring test environment');

class DBEnvironment extends NodeEnvironment {
  // eslint-disable-next-line no-useless-constructor
  constructor(config) {
    super(config);
  }

  async setup() {
    console.log('setting up test environment');
    this.global.pg = global.pg;
    await super.setup();
  }

  async teardown() {
    console.log('tearing down test environment');
    await super.teardown();
  }

  runScript(script) {
    return super.runScript(script);
  }
}

module.exports = DBEnvironment;
