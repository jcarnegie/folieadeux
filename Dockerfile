FROM node:9.5-alpine

RUN apk update
RUN apk add postgresql-dev
RUN apk add python
RUN apk add build-base
RUN apk add git

# to get flow to run on alpine:
RUN apk --no-cache add openssl
RUN wget -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.25-r0/glibc-2.25-r0.apk
RUN apk add glibc-2.25-r0.apk

RUN mkdir -p /app
WORKDIR /app

ARG NODE_ENV=development
ENV NODE_ENV $NODE_ENV

# RUN yarn global add node-gyp

ADD package.json yarn.lock ./
ADD tmp/docker/yarn-cache.tgz /

RUN yarn install

ADD . .

CMD yarnpkg start
