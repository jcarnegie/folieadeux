import { sign } from 'jsonwebtoken';
import { hashPassword, SECRET } from '../src/auth';

const login = async (_, { email, password }, { sequelize }) => {
  try {
    const { Person } = sequelize.models;
    const person = await Person.findOne({ where: { email } });
    const cryptedPassword = await hashPassword(password);

    if (person.cryptedPassword !== cryptedPassword) {
      throw new Error('Unable to login');
    }

    const tokenType = 'Bearer';
    const expiresIn = 300;
    const refreshExpiresIn = 60 * 60 * 24 * 30;
    const roles = person.roles || [];
    const personData = { id: person.id, roles };
    const accessToken = sign(personData, SECRET, { expiresIn });
    const refreshToken = sign(personData, SECRET, { expiresIn: refreshExpiresIn });
    return {
      accessToken,
      refreshToken,
      tokenType,
      expiresIn,
    };
  } catch (e) {
    throw new Error('Unable to login');
  }
};

export default login;
