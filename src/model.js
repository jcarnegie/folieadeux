// @flow
import type {
  DefinitionNode,
  DirectiveNode,
  DocumentNode,
  ObjectTypeDefinitionNode,
} from 'graphql/language';
import { parse } from 'graphql/language';
import {
  assoc,
  clone,
  contains,
  curry,
  filter,
  find,
  map,
  not,
  path,
  pathEq,
  prop,
  propEq,
  reduce,
} from 'ramda';
import { resolver as sequelizeResolver } from 'graphql-sequelize';
import { pluralize } from 'inflection';
import type { Relation } from './relation';
import type { Attribute } from './attribute';
import { createAttribute } from './attribute';
import { pathNotEq } from './util/object';

const DEFAULT_ALLOWED_OPS = ['list', 'create', 'view', 'update', 'delete'];

export type CrudOp = 'view' | 'list' | 'create' | 'update' | 'delete';

export type Model = {
  name: string,
  allowedOps: ?string[],
  attributes: ?Attribute[],
  // indexes: ?Index[],
  relations?: ?Relation[],
};

export const TYPE_MAP = {
  Boolean: { type: 'boolean' },
  Date: { type: 'date' },
  Float: { type: 'float' },
  ID: { type: 'bigint' },
  Int: { type: 'integer' },
  String: { type: 'varchar', size: 255 },
  Value: { type: 'text' },
};

export const isObjectTypeDef = (node: DefinitionNode): boolean =>
  node.kind === 'ObjectTypeDefinition';

export const findModelDirective = (node: ObjectTypeDefinitionNode): ?DirectiveNode =>
  find(pathEq(['name', 'value'], 'model'), node.directives || []);

export const isModel = (node: ObjectTypeDefinitionNode): boolean =>
  findModelDirective(node) !== undefined;

export const isObjectTypeDefModel = (node: ObjectTypeDefinitionNode): boolean =>
  (isObjectTypeDef(node) && isModel(node));

export const modelName = (node: ObjectTypeDefinitionNode): ?string => path(['name', 'value'], node);

export const modelAllowedOps = (node: ObjectTypeDefinitionNode): string[] => {
  const modelDirective = findModelDirective(node);
  const withArgument = find(pathEq(['name', 'value'], 'with'), modelDirective.arguments);
  if (!withArgument) return DEFAULT_ALLOWED_OPS;
  return map(prop('value'), withArgument.value.values);
};

export const findModelTypes = (ast: DocumentNode): $ReadOnlyArray<ObjectTypeDefinitionNode> =>
  filter(isObjectTypeDefModel, ast.definitions);

export const createModel = (node: ObjectTypeDefinitionNode): Model => {
  const name = modelName(node);
  const allowedOps = modelAllowedOps(node);
  return {
    name,
    allowedOps,
    attributes: map(createAttribute, node.fields),
  };
};

export const modelViewFieldDef = (node: ObjectTypeDefinitionNode): string => {
  const name = modelName(node);
  return `view${name}(id: ID!): ${name}`;
};

export const modelListFieldDef = (node: ObjectTypeDefinitionNode): string => {
  const name = modelName(node);
  return `list${name}(where: Value): [${name}]`;
};

export const modelQueryFieldDefs =
(node: ObjectTypeDefinitionNode): DocumentNode => {
  const viewDef = `type Query {
    ${modelViewFieldDef(node)}
    ${modelListFieldDef(node)}
  }`;
  return parse(viewDef);
};

export const modelCreateFieldDef = (node: ObjectTypeDefinitionNode): string => {
  const name = modelName(node);
  return `create${name}(payload: ${name}CreatePayload!): ${name}`;
};

export const modelUpdateFieldDef = (node: ObjectTypeDefinitionNode): string => {
  const name = modelName(node);
  return `update${name}(id: ID!, payload: ${name}UpdatePayload!): ${name}`;
};

export const modelDeleteFieldDef = (node: ObjectTypeDefinitionNode): string => {
  const name = modelName(node);
  return `delete${name} (id: ID!): Boolean`;
};

export const modelMutationFieldDefs =
(node: ObjectTypeDefinitionNode): DocumentNode => {
  const viewDef = `type Mutation {
    ${modelCreateFieldDef(node)}
    ${modelUpdateFieldDef(node)}
    ${modelDeleteFieldDef(node)}
  }`;
  return parse(viewDef);
};

export const createModelTypedef = curry((
  opType: 'Create' | 'Update',
  node: ObjectTypeDefinitionNode,
): ObjectTypeDefinitionNode => {
  const name: ?string = modelName(node);
  const copy = clone(node);
  copy.name.value = `${name}${opType}Payload`;
  // Set it as an input type in the AST
  copy.kind = 'InputObjectTypeDefinition';
  // strip model directive
  copy.directives = filter(pathNotEq(['name', 'value'], 'model'), copy.directives);
  if (opType === 'Create') {
    // strip id field
    copy.fields = filter(pathNotEq(['name', 'value'], 'id'), copy.fields);
  }
  // Set all fields as input types in AST - be sure to filter out relations
  const model: Model = createModel(node);
  const relationFields = map(prop('name'), filter(propEq('type', 'relation'), model.attributes));
  const nonRelationFields = filter(f => not(contains(f.name.value, relationFields)), copy.fields);
  copy.fields = map(assoc('kind', 'InputValueDefinition'), nonRelationFields);
  return copy;
});

export const createModelCreateTypedef = createModelTypedef('Create');

export const createModelUpdateTypedef = createModelTypedef('Update');

export const createResolver =
  curry((op: CrudOp, resolverFn: any, node: ObjectTypeDefinitionNode, sequelize: any): any => {
    const modelDef: Model = createModel(node);
    const model = sequelize.models[modelDef.name];
    const resolverKey = `${op}${modelDef.name}`;
    return {
      [resolverKey]: resolverFn(model, sequelize),
    };
  });

export const createModelViewResolver =
  createResolver('view', model => sequelizeResolver(model));

export const createModelListResolver =
  createResolver('list', model => sequelizeResolver(model));

export const createModelCreateResolver =
  createResolver('create', model => (_, { payload }) => model.create(payload));

export const createModelUpdateResolver =
  createResolver('update', model => (_, { id, payload }) => model.update({ id }, payload));

export const createModelDeleteResolver =
  createResolver('delete', model => (_, { id }) => model.destroy({ id }));

export const createModelFieldResolvers =
(node: ObjectTypeDefinitionNode, sequelize: any): any => {
  const modelDef: Model = createModel(node);
  const relationAttrs: Attribute[] = filter(propEq('type', 'relation'), modelDef.attributes);
  return reduce((acc, attr) => {
    const assocName = pluralize(attr.rawType);
    const association = sequelize.models[modelDef.name].associations[assocName];
    return { ...acc, [modelDef.name]: { [attr.name]: sequelizeResolver(association) } };
  }, {}, relationAttrs);
};
