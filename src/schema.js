// @flow

// Credit goes here for inspiration and implementation:
// https://gist.github.com/voodooattack/ce5f0afb5515ab5a153e535ac20698da

import type { DocumentNode, FieldDefinitionNode } from 'graphql/language';
import { parse } from 'graphql/language';
import { buildASTSchema } from 'graphql/utilities';
import { addResolveFunctionsToSchema } from 'graphql-tools';
import {
  assocPath,
  clone,
  compose,
  concat,
  curry,
  filter,
  flatten,
  forEach,
  isEmpty,
  map,
  pickAll,
  prop,
  pathEq,
  propEq,
  reduce,
} from 'ramda';
import { readdirSync } from 'fs';
import type { Model } from './model';
import {
  createModel,
  createModelViewResolver,
  createModelListResolver,
  createModelCreateResolver,
  createModelUpdateResolver,
  createModelDeleteResolver,
  createModelFieldResolvers,
  createModelCreateTypedef,
  createModelUpdateTypedef,
  findModelTypes,
  modelQueryFieldDefs,
  modelMutationFieldDefs,
  // modelsWithRelationships,
} from './model';
import { sequelizeAttributes } from './attribute';
import { pathNotMatch } from './util/object';

type Schema = string | DocumentNode;

const safeParse = schema => (typeof schema === 'string' ? parse(schema) : schema);

// $FlowFixMe
const isScalarType = t => propEq('kind', 'ScalarTypeDefinition', t);
// $FlowFixMe
const isObjectType = t => propEq('kind', 'ObjectTypeDefinition', t);
// $FlowFixMe
const isInputType = t => propEq('kind', 'InputObjectTypeDefinition', t);
const isQueryType = t => isObjectType(t) && pathEq(['name', 'value'], 'Query', t);
const isMutationType = t => isObjectType(t) && pathEq(['name', 'value'], 'Mutation', t);
const isSubscriptionType = t => isObjectType(t) && pathEq(['name', 'value'], 'Subscription', t);
const isNotSpecialObjectType = t => isObjectType(t) && pathNotMatch(/^(Query|Mutation|Subscription)$/, ['name', 'value'], t);

const findFields = curry(compose(map(prop('fields')), filter));

const filterQueryFields = findFields(isQueryType);
const filterMutationFields = findFields(isMutationType);
const filterSubscriptionFields = findFields(isSubscriptionType);
const filterScalarTypes = filter(isScalarType);
const filterInputTypes = filter(isInputType);
const filterObjectTypes = filter(isNotSpecialObjectType);

const FIELDS_PATH = ['definitions', 0, 'fields'];
// $FlowFixMe
const createQueryAst = fields => assocPath(FIELDS_PATH, fields, parse('type Query { _: Int }'));
// $FlowFixMe
const createMutationAst = fields => assocPath(FIELDS_PATH, fields, parse('type Mutation { _: Int }'));
// $FlowFixMe
const createSubscriptionAst = fields => assocPath(FIELDS_PATH, fields, parse('type Subscription { _: Int }'));

export const combineSchemas = (schemas: Schema[]): DocumentNode => {
  const result = { kind: 'Document', definitions: [] };
  const asts = map(safeParse, schemas);
  const definitions = flatten(map(prop('definitions'), asts));

  const queryFields: FieldDefinitionNode[] = flatten(filterQueryFields(definitions));
  const mutationFields: FieldDefinitionNode[] = flatten(filterMutationFields(definitions));
  const subscriptionFields: FieldDefinitionNode[] = flatten(filterSubscriptionFields(definitions));

  const mutationTypes = createMutationAst(mutationFields);
  const queryTypes = createQueryAst(queryFields);
  const subscriptionTypes = createSubscriptionAst(subscriptionFields);
  const scalarTypes = flatten(filterScalarTypes(definitions));
  const inputTypes = flatten(filterInputTypes(definitions));
  const types = flatten(filterObjectTypes(definitions));

  result.definitions = concat(result.definitions, scalarTypes);
  result.definitions = concat(result.definitions, types);
  result.definitions = concat(result.definitions, inputTypes);
  if (!isEmpty(queryTypes.definitions[0].fields)) {
    result.definitions = concat(result.definitions, queryTypes.definitions);
  }
  if (!isEmpty(mutationTypes.definitions[0].fields)) {
    result.definitions = concat(result.definitions, mutationTypes.definitions);
  }
  if (!isEmpty(subscriptionTypes.definitions[0].fields)) {
    result.definitions = concat(result.definitions, subscriptionTypes.definitions);
  }
  return result;
};

export const enhanceSchemaWithResolverTypes =
(ast: DocumentNode): DocumentNode => {
  const modelTypes = findModelTypes(ast);
  const querySchemas: DocumentNode[] = map(modelQueryFieldDefs, modelTypes);
  const mutationSchemas: DocumentNode[] = map(modelMutationFieldDefs, modelTypes);
  return combineSchemas([ast, ...querySchemas, ...mutationSchemas]);
};

export const enhanceSchemaWithInputTypes =
(ast: DocumentNode): DocumentNode => {
  const enhancedAst: any = clone(ast);
  const modelTypes = findModelTypes(enhancedAst);
  const createInputTypes = map(createModelCreateTypedef, modelTypes);
  const updateInputTypes = map(createModelUpdateTypedef, modelTypes);
  const combinedInputTypes = concat(createInputTypes, updateInputTypes);
  enhancedAst.definitions = concat(combinedInputTypes, enhancedAst.definitions);
  return enhancedAst;
};

export const enhanceSchemaWithCustomTypes =
(ast: DocumentNode, typesDir: string = `${__dirname}/types`): DocumentNode => {
  /* eslint-disable global-require,import/no-dynamic-require */
  // $FlowFixMe
  const types = map(f => require(`${typesDir}/${f}`).default, readdirSync(typesDir));
  /* eslint-enable global-require,import/no-dynamic-require */
  const scalarsSchema = map(t => `scalar ${t.name}\n`, types);
  return combineSchemas([ast, ...scalarsSchema]);
};

export const createCustomTypeResolvers = (typesDir: string = `${__dirname}/types`): any => {
  /* eslint-disable global-require,import/no-dynamic-require */
  // $FlowFixMe
  const types = map(f => require(`${typesDir}/${f}`).default, readdirSync(typesDir));
  /* eslint-enable global-require,import/no-dynamic-require */
  return reduce((resolvers, t) => ({ ...resolvers, [t.name]: t }), {}, types);
};

export const createResolvers = (ast: DocumentNode, sequelize: any): any => {
  const models = findModelTypes(ast);
  const initialResolvers = createCustomTypeResolvers();
  return reduce((resolvers, model) => ({
    Query: {
      ...resolvers.Query,
      ...createModelViewResolver(model, sequelize),
      ...createModelListResolver(model, sequelize),
    },
    Mutation: {
      ...resolvers.Mutation,
      ...createModelCreateResolver(model, sequelize),
      ...createModelUpdateResolver(model, sequelize),
      ...createModelDeleteResolver(model, sequelize),
    },
    ...createModelFieldResolvers(model, sequelize),
  }), { ...initialResolvers, Query: {}, Mutation: {} }, models);
};

export const sequelizeModels = (ast: DocumentNode, sequelize: any) => {
  const modelTypes = findModelTypes(ast);
  const models: Model[] = map(createModel, modelTypes);
  // create models in sequelize
  map(m => sequelize.define(m.name, sequelizeAttributes(m)), models);
  // create / wire up relations in sqeuelize
  forEach((m) => {
    forEach((a: Attribute) => {
      if (a.relation) {
        const r = a.relation;
        const model = sequelize.models[m.name];
        const otherModel = sequelize.models[r.target];
        const options = pickAll(['as', 'through', 'foreignKey', 'otherKey'], r);
        // create sequelize relationship
        model[r.type](otherModel, options);
      }
    }, m.attributes || []);
  }, models);
};

export const build = (schemas: Schema[], sequelize: any) => {
  let schema: DocumentNode = combineSchemas(schemas);
  schema = enhanceSchemaWithResolverTypes(schema);
  schema = enhanceSchemaWithInputTypes(schema);
  schema = enhanceSchemaWithCustomTypes(schema);
  sequelizeModels(schema, sequelize);
  const resolvers = createResolvers(schema, sequelize);
  const builtSchema = buildASTSchema(schema);
  addResolveFunctionsToSchema(builtSchema, resolvers);
  return builtSchema;
};
