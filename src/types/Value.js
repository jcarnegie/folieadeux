import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import { identity } from 'ramda';

export default new GraphQLScalarType({
  name: 'Value',
  description: 'Value Type',
  serialize: identity,
  parseValue: identity,
  parseLiteral({
    kind,
    value,
    values,
    fields,
  }) {
    switch (kind) {
      case Kind.STRING:
      case Kind.BOOLEAN:
        return value;
      case Kind.INT:
      case Kind.FLOAT:
        return parseFloat(value);
      case Kind.OBJECT: {
        const objValue = Object.create(null);
        fields.forEach((field) => {
          objValue[field.name.value] = this.parseValueLiteral(field.value);
        });
        return objValue;
      }
      case Kind.LIST:
        return values.map(this.parseValueLiteral);
      default:
        return null;
    }
  },
});
