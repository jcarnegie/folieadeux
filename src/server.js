// @flow
import Sequelize from 'sequelize';
import { GraphQLServer } from 'graphql-yoga';
import { keys } from 'ramda';
import { build } from './schema';

const start = async () => {
  try {
    const typedefs = `
      type Blah {
        foo: String
        bar: Int
      }

      type Foo @model {
        id: ID! @serialize
        ha: String
        fa: Int
      }

      type Bar @model {
        id: ID! @serialize
        maid: String
        foo: Foo @relation(type: "hasMany")
      }

      type User @model {
        id: ID! @primary @increments
        email: String! @unique
        phone: String
        bars: [Bar] @relation(type: "belongsToMany", through: "UserBar")
      }
    `;

    const sequelize = new Sequelize('postgres', 'postgres', '', { dialect: 'postgres' });
    const schema = build([typedefs], sequelize);

    const server = new GraphQLServer({
      schema,
      // context: async ({ request }) => {
      //   try {
      //     const person = await loggedInPersonFromAuthHeader(request);
      //     return { person, request, sequelize };
      //   } catch (tokenError) {
      //     return {
      //       person: null,
      //       request,
      //       sequelize,
      //       tokenError,
      //     };
      //   }
      // },
    });

    // migrate the schema
    console.log('migrating schema');
    // await sequelize.sync({ alter: true });
    await sequelize.sync();

    console.log('starting server');
    server.start(() => console.log('Server is running on localhost:4000'));
  } catch (e) {
    console.log(e);
  }
};

start();
