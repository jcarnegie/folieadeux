import {
  compose,
  curry,
  isEmpty,
  match,
  not,
  path,
  pathEq,
  propEq,
} from 'ramda';

export const pathNotEq = curry(compose(not, pathEq));

export const pathMatch = (re, pathArr, obj) =>
  not(isEmpty(match(re, path(pathArr, obj))));

export const pathNotMatch = curry(compose(not, pathMatch));

export const propNotEq = curry(compose(not, propEq));