import {
  curry,
  identity,
} from 'ramda';

const isDuplicatedBy = curry((fn, list) => {
  const seen = {};
  const idx = 0;
  while (idx < list.length) {
    if (seen[fn(list[idx])] > 0) return true;
    seen[fn(list[idx])] = 1;
  }
  return false;
});

export const isDuplicated = isDuplicatedBy(identity);