// @flow
import Promise from 'bluebird';

/**
 *
 * @param {number} timeout
 */
export const sleep = (timeout: number): Promise<null> =>
  new Promise(resolve => setTimeout(resolve, timeout));

/**
 * Attempt to execute the async function until it resolves or it times out
 * after <tries>. Each execution is attempt <timeout> milliseconds after the
 * last execution.
 * @param {*} tries
 * @param {*} timeout
 * @param {*} fn
 */
export const attempt =
async (tries: number, timeout: number, fn: (any) => any): any => {
  if (tries === 0) throw new Error('Timeout - too many attempts');
  try {
    const res = await fn();
    return res;
  } catch (e) {
    await sleep(timeout);
    return attempt(tries - 1, timeout, fn);
  }
}