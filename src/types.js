import Sequelize from 'sequelize';
import { isEmpty, match } from 'ramda';

export const sqlToSequelize = (sqlType) => {
  let matches = null;

  // varchar(N)
  matches = match(/^\s*varchar\s*(\(\s*(\d+)\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    return (matches[2])
      ? Sequelize.STRING(parseInt(matches[2], 10))
      : Sequelize.STRING;
  }

  // varchar binary(N)
  matches = match(/^\s*varchar\s*(\(\s*(\d+)\s*\))?\s*binary\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    return (matches[2])
      ? Sequelize.STRING(parseInt(matches[2], 10)).BINARY
      : Sequelize.STRING.BINARY;
  }

  // text
  matches = match(/^\s*text\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.TEXT;

  // tinytext
  matches = match(/^\s*tinytext\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.TEXT('tiny');

  // integer
  matches = match(/^\s*integer\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.INTEGER;

  // bigint
  matches = match(/^\s*bigint\s*(\(\s*(\d+)\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    return (matches[2])
      ? Sequelize.BIGINT(parseInt(matches[2], 10))
      : Sequelize.BIGINT;
  }

  // float
  matches = match(/^\s*float\s*(\(\s*(\d+)(\s*,\s*(\d+))?\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    if (matches[2] && matches[4]) {
      return Sequelize.FLOAT(parseInt(matches[2], 10), parseInt(matches[2], 10));
    }
    return (matches[2])
      ? Sequelize.FLOAT(parseInt(matches[2], 10))
      : Sequelize.FLOAT;
  }

  // real
  matches = match(/^\s*real\s*(\(\s*(\d+)(\s*,\s*(\d+))?\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    if (matches[2] && matches[4]) {
      return Sequelize.REAL(parseInt(matches[2], 10), parseInt(matches[2], 10));
    }
    return (matches[2])
      ? Sequelize.REAL(parseInt(matches[2], 10))
      : Sequelize.REAL;
  }

  // double
  matches = match(/^\s*double\s*(\(\s*(\d+)(\s*,\s*(\d+))?\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    if (matches[2] && matches[4]) {
      return Sequelize.DOUBLE(parseInt(matches[2], 10), parseInt(matches[2], 10));
    }
    return (matches[2])
      ? Sequelize.DOUBLE(parseInt(matches[2], 10))
      : Sequelize.DOUBLE;
  }

  // decimal
  matches = match(/^\s*decimal\s*(\(\s*(\d+)\s*,\s*(\d+)\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    if (matches[2] && matches[3]) {
      return Sequelize.DECIMAL(parseInt(matches[2], 10), parseInt(matches[2], 10));
    }
    return Sequelize.DECIMAL;
  }

  // timestamp with time zone
  matches = match(/^\s*timestamp\s*with\s*time\s*zone\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.DATE;

  // datetime
  matches = match(/^\s*datetime\s*(\(\s*(\d+)\s*\))?\s*$/i, sqlType);
  if (!isEmpty(matches)) {
    if (matches[2]) {
      if (parseInt(matches[2], 10) < 0 || parseInt(matches[2], 10) > 6) {
        throw new Error('datetime modifier must be between 0 and 6');
      }
      return Sequelize.DATE(parseInt(matches[2], 10));
    }
    return Sequelize.DATE;
  }

  // json
  matches = match(/^\s*json\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.JSON;

  // jsonb
  matches = match(/^\s*jsonb\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.JSONB;

  // blob / bytea
  matches = match(/^\s*(blob|bytea)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.BLOB;

  // tinyblob
  matches = match(/^\s*(tinyblob)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.BLOB('tiny');

  // mediumblob
  matches = match(/^\s*(mediumblob)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.BLOB('medium');

  // longblob
  matches = match(/^\s*(longblob)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.BLOB('long');

  // uuid
  matches = match(/^\s*(uuid)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.UUID;

  // geometry
  matches = match(/^\s*(geometry)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.GEOMETRY;

  // point
  matches = match(/^\s*(point)\s*$/i, sqlType);
  if (!isEmpty(matches)) return Sequelize.GEOMETRY('point');

  throw new Error(`Unknown type '${sqlType}'`);
};
