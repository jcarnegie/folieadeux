// @flow
import {
  append,
  assoc,
  contains,
  differenceWith,
  filter,
  find,
  join,
  keys,
  map,
  pickAll,
  prop,
  propEq,
  reduce,
  toLower,
  values,
} from 'ramda';
import { intersectionWith, renameKeys } from './util';
import { build as buildSchema } from './schema';

export type ColumnType =
  | 'smallint'
  | 'integer'
  | 'bigint'
  | 'decimal'
  | 'numeric'
  | 'real'
  | 'double precision'
  | 'smallserial'
  | 'serial'
  | 'bigserial'
  | 'character varying'
  | 'character';

export type Nullable = 'YES' | 'NO';

export type ColumnDefinition = {
  name: string,
  type: ColumnType,
  length?: number,
  precision?: number,
  scale?: number,
  nullable?: Nullable,
}

export type TableDefinition = {
  name: string,
  columns: [ColumnDefinition],
};

export type MigrateCommandType =
  | "run"
  | "log";

export type MigrateCommand = {
  sql: string,
  type: MigrateCommandType,
};

const METADATA_SQL = `
SELECT
  columns.*

  --columns.table_name,
  --columns.column_name,
  --columns.data_type,
  --columns.character_maximum_length,
  --columns.character_octet_length,
  --columns.numeric_precision,
  --columns.numeric_precision_radix,
  --columns.numeric_scale,
  --columns.datetime_precision,
  --columns.interval_type,
  --columns.interval_precision,
  --columns.is_identity,
  --columns.column_default,
  --columns.is_nullable
FROM information_schema.columns
WHERE columns.table_schema='public';
`;

const columnMap = {
  column_name: 'name',
  column_default: 'defaultValue',
  data_type: 'type',
  character_maximum_length: 'length',
  numeric_precision: 'precision',
  numeric_scale: 'scale',
  is_nullable: 'allowNull',
};

export const databaseSchema = async (client): [TableDefinition] => {
  const result = await client.query(METADATA_SQL);
  // console.log(result);
  const tableDefs = reduce((defs, col) => {
    const tableDef = defs[col.table_name] || {};
    const existingColumns = tableDef.columns || [];
    tableDef.name = col.table_name;
    tableDef.columns =
      append(
        pickAll(
          values(columnMap),
          renameKeys(columnMap, col),
        ),
        existingColumns,
      );

    tableDef.columns = map((colDef) => {
      const updatedColDef = colDef;
      if (colDef.length) updatedColDef.type = `${colDef.type}(${colDef.length})`;
      if (contains(updatedColDef.type, ['decimal', 'numeric'])) {
        let precisionAndScale = '';
        if (colDef.precision) {
          precisionAndScale = `(${colDef.precision}`;
          if (colDef.scale) {
            precisionAndScale += `,${colDef.scale}`;
          }
          precisionAndScale += ')';
          updatedColDef.type += precisionAndScale;
        }
      }
      // eslint-disable-next-line no-unneeded-ternary
      updatedColDef.allowNull = (updatedColDef.allowNull === 'YES') ? true : false;
      return updatedColDef;
    }, tableDef.columns);

    return assoc(col.table_name, tableDef, defs);
  }, {}, result.rows);
  return values(tableDefs);
};


// eslint-disable-next-line import/prefer-default-export
export const sequelizeSchema = async (sequelize, schemaFragments) => {
  try {
    buildSchema({ schemaFragments, sequelize });

    const gqlTables = map(m => ({
      name: m.getTableName(),
      columns: map(
        col => ({
          name: col,
          type: toLower(m.attributes[col].type.toString()),
          unique: m.attributes[col].unique,
          primaryKey: m.attributes[col].primaryKey,
          autoIncrement: m.attributes[col].autoIncrement,
          allowNull: m.attributes[col].allowNull,
          defaultValue: m.attributes[col].defaultValue,
        }),
        keys(m.attributes),
      ),
    }), values(sequelize.models));

    return gqlTables;
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
};

const serialTypes = {
  smallint: 'smallserial',
  integer: 'serial',
  bigint: 'bigserial',
};

export const serialType = t => serialTypes(t) || t;

export const columnSQL = (c: ColumnDefinition): string => {
  const type = (c.autoIncrement) ? serialType(c.type) : c.type;
  let sql = `"${c.name}" ${type}`;
  if (c.unique) sql += ' unique';
  if (c.primaryKey) sql += ' primary key';
  if (c.defaultValue) sql += ` default ${c.defaultValue}`;
  sql += c.allowNull ? ' null' : ' not null';
  return sql;
};

export const createTableSQL = (tableDef: TableDefinition): string => `
create table "${tableDef.name}" (
  ${join(',\n  ', map(columnSQL, tableDef.columns))}
);`;

export const addColumnSQL = (
  tableDef: TableDefinition,
  columnDef: ColumnDefinition,
): string => `
alter table "${tableDef.name}" add column ${columnSQL(columnDef)};
`;

export const alterColumnTypeSQL = (
  tableDef: TableDefinition,
  columnDef: ColumnDefinition,
): string => `
alter table "${tableDef.name}" alter column "${columnDef.name}" type ${columnDef.type};
`;

export const alterColumnDefaultSQL = (
  tableDef: TableDefinition,
  columnDef: ColumnDefinition,
): string => {
  const { defaultValue } = columnDef;
  return `alter table "${tableDef.name}" alter column "${columnDef.name}" set default '${defaultValue}';`;
};

export const alterColumnDropDefaultSQL = (
  tableDef: TableDefinition,
  columnDef: ColumnDefinition,
): string => `alter table "${tableDef.name}" alter column "${columnDef.name}" drop default;
`;

export const alterColumnSetNotNullSQL = (
  tableDef: TableDefinition,
  columnDef: ColumnDefinition,
): string => `alter table "${tableDef.name}" alter column "${columnDef.name}" set not null;
`;

export const alterColumnDropNotNullSQL = (
  tableDef: TableDefinition,
  columnDef: ColumnDefinition,
): string => `alter table "${tableDef.name}" alter column "${columnDef.name}" drop not null;
`;

export const addNewColumns = (client, dbSchema, seqSchema) =>
  Promise.all(map(async (tableDef) => {
    //
    const colDefCmp = (x: ColumnDefinition, y: ColumnDefinition): Boolean => x.name === y.name;
    const dbTableDef = find(propEq('name', tableDef.name), dbSchema);
    const newColumns = differenceWith(colDefCmp, tableDef.columns, dbTableDef.columns);

    const promises = map((colDef: ColumnDefinition): Promise => {
      const sql = addColumnSQL(tableDef, colDef);
      return client.query(sql);
    }, newColumns);
    return Promise.all(promises);
  }, seqSchema));

export const migrateExistingColumns = (client, dbSchema, seqSchema) =>
  Promise.all(map(async (tableDef) => {
    //
    const colDefCmp = (x: ColumnDefinition, y: ColumnDefinition): Boolean =>
      x.name === y.name && x.type !== y.type;
    const dbTableDef = find(propEq('name', tableDef.name), dbSchema);
    const changedColumns = intersectionWith(colDefCmp, tableDef.columns, dbTableDef.columns);
    const changedColumnNames = map(prop('name'), changedColumns);
    const columnsToChange = filter(
      colDef => contains(colDef.name, changedColumnNames),
      tableDef.columns,
    );

    const promises = map((colDef: ColumnDefinition): Promise => {
      // Disallowed column alteration types:
      //    varchar --> bytea
      //    varchar --> integer
      //    varchar --> bigint
      const sql = alterColumnTypeSQL(tableDef, colDef);
      return client.query(sql);
    }, columnsToChange);
    return Promise.all(promises);
  }, seqSchema));

export const updateColumnDefaults = (client, dbSchema, seqSchema) =>
  Promise.all(map(async (tableDef) => {
    //
    const colDefCmp = (x: ColumnDefinition, y: ColumnDefinition): Boolean =>
      x.name === y.name && x.defaultValue !== y.defaultValue;
    const dbTableDef = find(propEq('name', tableDef.name), dbSchema);
    const changedColumns = intersectionWith(colDefCmp, tableDef.columns, dbTableDef.columns);
    const changedColumnNames = map(prop('name'), changedColumns);
    const columnsToChange = filter(
      colDef => contains(colDef.name, changedColumnNames) && !colDef.autoIncrement,
      tableDef.columns,
    );

    const promises = map((colDef: ColumnDefinition): Promise => {
      const sql = (colDef.defaultValue)
        ? alterColumnDefaultSQL(tableDef, colDef)
        : alterColumnDropDefaultSQL(tableDef, colDef);
      return client.query(sql);
    }, columnsToChange);
    return Promise.all(promises);
  }, seqSchema));

export const updateColumnNullableConstraints = (client, dbSchema, seqSchema) =>
  Promise.all(map(async (tableDef) => {
    //
    const colDefCmp = (x: ColumnDefinition, y: ColumnDefinition): Boolean =>
      x.name === y.name && x.allowNull !== y.allowNull;
    const dbTableDef = find(propEq('name', tableDef.name), dbSchema);
    const changedColumns = intersectionWith(colDefCmp, tableDef.columns, dbTableDef.columns);
    const changedColumnNames = map(prop('name'), changedColumns);
    const columnsToChange = filter(
      colDef => contains(colDef.name, changedColumnNames) && !colDef.primaryKey,
      tableDef.columns,
    );

    const promises = map((colDef: ColumnDefinition): Promise => {
      const sql = (colDef.allowNull)
        ? alterColumnDropNotNullSQL(tableDef, colDef)
        : alterColumnSetNotNullSQL(tableDef, colDef);
      return client.query(sql);
    }, columnsToChange);
    return Promise.all(promises);
  }, seqSchema));

export const migrate = async (client, sequelize, schemaFragments) => {
  try {
    // this will add missing tables
    await sequelize.sync({ logging: false });

    // get parsed db schema
    const dbSchema = await databaseSchema(client);

    // get parsed graphql schema
    const seqSchema = await sequelizeSchema(sequelize, schemaFragments);

    // this will add new coluns in the GraphQL schema to the DB schema
    await addNewColumns(client, dbSchema, seqSchema);

    // this will migrate existing columns to new SQL types in the DB schema
    // (i.e. types updated in the GraphQL schema will update types in the DB
    // schema)
    await migrateExistingColumns(client, dbSchema, seqSchema);

    // this will update column defaults
    await updateColumnDefaults(client, dbSchema, seqSchema);

    // update null / not null constraints
    await updateColumnNullableConstraints(client, dbSchema, seqSchema);
  } catch (e) {
    throw e;
  }
};
