// @flow

// import Sequelize from 'sequelize';
// import hashObject from 'object-hash';
// import { Client } from 'pg';
// import { sleep } from './util';

const Sequelize = require('sequelize');
const hashObject = require('object-hash');
const Promise = require('bluebird');
const { Client } = require('pg');

const sleep = timeout =>
  new Promise(resolve => setTimeout(resolve, timeout));

/**
 * Factory for database / sequelize connections
 */

exports.DIALECT = process.env.DB_DIALECT || 'postgres';
exports.user = process.env.DB_USERNAME || 'postgres';
exports.password = process.env.DB_PASSWORD || 'password';
exports.host = process.env.DB_HOST || '127.0.0.1';
exports.port = process.env.DB_PORT || 5432;
exports.database = process.env.DB_DATABASE || 'postgres';

const pgConnections = {};

const connect = async (
  connectInfo = {},
  attempts = 20,
  interval = 1000,
) => {
  // const connectionId = hashObject({
  //   user: connectInfo.user || exports.user,
  //   password: connectInfo.password || exports.password,
  //   host: connectInfo.host || exports.host,
  //   port: connectInfo.port || exports.port,
  //   database: connectInfo.database || exports.database,
  // });

  // if (pgConnections[connectionId]) return pgConnections[connectionId];

  if (attempts === 0) throw new Error('Failed to connect');
  try {
    const client = new Client({
      user: connectInfo.user || exports.user,
      password: connectInfo.password || exports.password,
      host: connectInfo.host || exports.host,
      port: connectInfo.port || exports.port,
      database: connectInfo.database || exports.database,
    });
    await client.connect();
    // pgConnections[connectionId] = client;
    return client;
  } catch (e) {
    await sleep(interval);
    return connect(connectInfo, attempts - 1, interval);
  }
};

exports.connect = connect;

const sequelizes = {};

exports.getSequelize = (
  theDatabase = exports.database,
  theUser = exports.user,
  thePassword = exports.password,
  other = { dialect: DIALECT },
) => {
  const connectionId = hashObject({
    theDatabase,
    theUser,
    thePassword,
    other,
  });

  if (sequelizes[connectionId]) return sequelizes[connectionId];

  return new Sequelize(
    theDatabase,
    theUser,
    thePassword,
    other,
  );
};

exports.getTableMetadata = async (client, tableName) => {
  /**
   * Need this info:
   *
   * columns
   * - type
   * - length
   * - precision
   * - scale
   * - null / not null
   * - default value

   * indexes
   * - primary key
   * - unique
   * - foreign key
   */
  const sql = `
select
  column_name,
  data_type,
  column_default,
  is_nullable,
  character_maximum_length,
  numeric_precision,
  numeric_scale
from
  information_schema.columns
where
  table_name=$1
`;

  return client.query(sql, [tableName]);
};
