// @flow

import type { ArgumentNode, FieldDefinitionNode, ValueNode } from 'graphql/language';
import {
  assoc,
  contains,
  filter,
  find,
  map,
  merge,
  mergeAll,
  path,
  pathEq,
  reduce,
} from 'ramda';
import { createRelation } from './relation';
import { sqlToSequelize } from './types';
import { propNotEq } from './util/object';

export type Attribute = {
  name: string,
  type: string,
  rawType: string,
  sequelizeType?: any,
  primaryKey?: boolean,
  autoIncrement?: boolean,
  allowNull?: boolean,
  size?: number,
  precision?: number,
  scale?: number,
  unique?: boolean,
  field?: string,
  default?: string,
  relation?: Relation
};

export const SQL_DIRECTIVE_ARGS = [
  'type',
  'size',
];

export const TYPE_MAP = {
  Boolean: { type: 'boolean' },
  Date: { type: 'date' },
  Float: { type: 'float' },
  ID: { type: 'bigint' },
  Int: { type: 'integer' },
  String: { type: 'varchar', size: 255 },
  Value: { type: 'text' },
};

export const fieldName = (field: FieldDefinitionNode): string => field.name.value;

export const isFieldNonNullType = (field: FieldDefinitionNode) => field.type.kind === 'NonNullType';

export const isListType = (field: FieldDefinitionNode) => field.type.kind === 'ListType';

export const fieldType = (field: FieldDefinitionNode): string => {
  const typePath = (isFieldNonNullType(field) || isListType(field))
    ? ['type', 'type', 'name', 'value']
    : ['type', 'name', 'value'];
  return path(typePath, field);
};

export const hasPrimaryDirective = (field: FieldDefinitionNode): boolean =>
  find(pathEq(['name', 'value'], 'primary'), field.directives) !== undefined;

export const hasIncrementsDirective = (field: FieldDefinitionNode): boolean =>
  find(pathEq(['name', 'value'], 'increments'), field.directives) !== undefined;

export const hasSerializeDirective = (field: FieldDefinitionNode): boolean =>
  find(pathEq(['name', 'value'], 'serialize'), field.directives) !== undefined;

export const isUniqueDirective = (field: FieldDefinitionNode): boolean =>
  find(pathEq(['name', 'value'], 'unique'), field.directives) !== undefined;

export const hasRelationDirective = (field: FieldDefinitionNode): boolean =>
  find(pathEq(['name', 'value'], 'relation'), field.directives) !== undefined;

export const relationDirective = (field: FieldDefinitionNode): boolean =>
  find(pathEq(['name', 'value'], 'relation'), field.directives);

export const coerceArgValue = (value: ValueNode): any => {
  switch (value.kind) {
    case 'IntValue': return parseInt(value.value, 10);
    case 'FloatValue': return parseFloat(value.value);
    case 'StringValue': return value.value;
    case 'BooleanValue': return value.value;
    case 'NullValue': return null;
    case 'ListValue': return map(coerceArgValue, value.values);
    case 'ObjectValue': return reduce((acc, field) =>
      merge(acc, { [field.name.value]: coerceArgValue(field.value) }), {}, value.fields);
    default:
      throw new Error(`Unknown directive argument type '${value.kind}'`);
  }
};

export const sqlDirective = (field: FieldDefinitionNode): any => {
  const sql = find(pathEq(['name', 'value'], 'sql'), field.directives);
  if (!sql) return {};
  const reduceFn = (acc, arg: ArgumentNode) =>
    merge(
      acc,
      // only include valid arguments
      contains(arg.name.value, SQL_DIRECTIVE_ARGS)
        ? { [arg.name.value]: coerceArgValue(arg.value) }
        : {},
    );
  return reduce(reduceFn, {}, sql.arguments);
};

export const createAttribute = (field: FieldDefinitionNode): Attribute => {
  const name = fieldName(field);
  const type = fieldType(field);
  const rawType = fieldType(field);
  const allowNull = !isFieldNonNullType(field);
  const sqlMetadata = sqlDirective(field);
  const serialize = hasSerializeDirective(field);
  const unique = isUniqueDirective(field);
  let primaryKey = hasPrimaryDirective(field);
  let autoIncrement = hasIncrementsDirective(field);

  if (serialize) {
    primaryKey = true;
    autoIncrement = true;
  }

  // Todo: make portable
  const typeObj = TYPE_MAP[type] || { type: 'jsonb' };

  // Todo: if isListType(field), then make type == 'jsonb'?

  // Todo:
  //  hiddenColumn
  //  unique
  //  notNull
  //  jsonb (could do as @sql(type: "jsonb"))
  //  real
  //  decimal
  //  virtual

  const attr: Attribute = mergeAll([
    { name },
    typeObj,
    { sequelizeType: sqlToSequelize(typeObj.type) },
    {
      rawType,
      allowNull,
      autoIncrement,
      primaryKey,
      unique,
    },
    sqlMetadata,
  ]);

  if (hasRelationDirective(field)) {
    attr.type = 'relation';
    const relDir = relationDirective(field);
    return assoc('relation', createRelation(attr, relDir), attr);
  }

  return attr;
};

export const sequelizeAttributes = (model: Model): Attribute[] =>
  reduce(
    (attrs, a) => assoc(a.name, { ...a, type: a.sequelizeType }, attrs),
    {},
    filter(propNotEq('type', 'relation'), model.attributes),
  );
