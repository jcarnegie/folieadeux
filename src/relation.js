// @flow

import type { DirectiveNode } from 'graphql/language';
import { contains, map, merge, reduce } from 'ramda';

export type RelationType =
  'belongsTo'
  | 'hasOne'
  | 'hasMany'
  | 'belongsToMany';

export type Relation = {
  type: RelationType,
  target: string,
  as?: string,
  through?: string,
  foreignKey?: string,
  otherKey?: string,
};

export const VALID_KEYS = ['type', 'as', 'through', 'foreignKey', 'otherKey'];

export const coerceArgValue = (value: ValueNode): any => {
  switch (value.kind) {
    case 'IntValue': return parseInt(value.value, 10);
    case 'FloatValue': return parseFloat(value.value);
    case 'StringValue': return value.value;
    case 'BooleanValue': return value.value;
    case 'NullValue': return null;
    case 'ListValue': return map(coerceArgValue, value.values);
    case 'ObjectValue': return reduce((acc, field) =>
      merge(acc, { [field.name.value]: coerceArgValue(field.value) }), {}, value.fields);
    default:
      throw new Error(`Unknown directive argument type '${value.kind}'`);
  }
};

export const createRelation = (attr: Attribute, directive?: DirectiveNode = []): Relation => {
  const base = { target: attr.rawType };
  const reduceFn = (acc, arg: ArgumentNode) =>
    merge(
      acc,
      // only include valid arguments
      contains(arg.name.value, VALID_KEYS)
        ? { [arg.name.value]: coerceArgValue(arg.value) }
        : {},
    );
  return reduce(reduceFn, base, directive.arguments);
};
